FROM alpine:3

ARG BTRBK_VERSION="v0.30.0"
ARG BTRBK_DIST_URL="https://raw.githubusercontent.com/digint/btrbk"

RUN apk update && \
  apk add btrfs-progs perl ca-certificates mbuffer curl sudo openssh-client

ARG BTRBK_HOME="/opt/btrbk"
RUN addgroup -g 1111 btrbk && \
  adduser -u 1111 -h /opt/btrbk -D -G btrbk btrbk && \
  echo "%btrbk ALL=(ALL) NOPASSWD:$(command -v readlink),$(command -v btrfs)" > /etc/sudoers.d/btrbk


# USER btrbk

RUN mkdir -p /opt/btrbk/.ssh
RUN curl ${BTRBK_DIST_URL}/${BTRBK_VERSION}/btrbk -o /opt/btrbk/btrbk && \
  curl ${BTRBK_DIST_URL}/${BTRBK_VERSION}/ssh_filter_btrbk.sh -o /opt/btrbk/ssh_filter_btrbk.sh && \
  chmod +x /opt/btrbk/*

ENTRYPOINT [ "/opt/btrbk/btrbk" ]
CMD [ "--help" ]
